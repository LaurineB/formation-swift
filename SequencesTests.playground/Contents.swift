//: Playground - noun: a place where people can play

import UIKit

protocol ItemDataShareable {
    var coid: Int? { get }
}


extension ItemDataShareable {
    func compareTo(_ other: ItemDataShareable) -> Bool {
        return self.coid == other.coid
    }
}

struct Article : ItemDataShareable {
    var coid: Int?
}

extension Sequence where Iterator.Element == ItemDataShareable {
    
    /*
     *  Comparison between 2 arrays of ItemDataShareable elements
     */
    func compareTo <T>(_ array: T) -> Bool where T : Sequence, T.Element == ItemDataShareable {
        
        
        return true
    }
}

let array1 : [ItemDataShareable] = [Article(coid: 1), Article(coid: 4), Article(coid: 3)]
let array2 : [ItemDataShareable] = [Article(coid: 1), Article(coid: 2), Article(coid: 3)]

let sequence1 = AnySequence(array1)
let sequence2 = AnySequence(array2)

array1.compareTo(array2)
sequence1.compareTo(array2)
array1.compareTo(sequence1)
sequence1.compareTo(sequence2)
