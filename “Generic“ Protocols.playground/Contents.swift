//: Playground - noun: a place where people can play

import UIKit

protocol Additioning {
    
    associatedtype Element
    
    func add(_ lhs: Element, _ rhs: Element) -> Element
}

struct AnyAdditioner<Element> : Additioning {
    
    let base: (Element, Element) -> Element
    
    init(_ base: @escaping (Element, Element) -> Element) {
        self.base = base
    }
    
    init<A>(_ base: A) where A : Additioning, A.Element == Element {
        self.base = base.add
    }

    
    func add(_ lhs: Element, _ rhs: Element) -> Element {
        return base(lhs, rhs)
    }
}

struct IntAdditioner : Additioning {
    typealias Operand = Int
    
    func add(_ lhs: Int, _ rhs: Int) -> Int {
        return lhs + rhs
    }
}

struct IntNotReallyAdditioner : Additioning {
    typealias Operand = Int
    
    func add(_ lhs: Int, _ rhs: Int) -> Int {
        return lhs - rhs
    }
}

let additioner: AnyAdditioner<Int> = AnyAdditioner(IntNotReallyAdditioner())
additioner.add(4, 5)
