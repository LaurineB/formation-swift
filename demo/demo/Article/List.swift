//
//  List.swift
//  demo
//
//  Created by Laurine Baillet on 28/11/2017.
//  Copyright © 2017 Laurine Baillet. All rights reserved.
//

class List<Element> {
    var head: Element?
    var tail: List?
}
// type Any sequence : type est là pour effacer une information d'un protocole
let sequence : AnySequence<Int> = AnySequence { AnyIterator { nil }}

let array = [1, 2, 3, 4]
let set : Set<Int> = [1,3,4]
// on masque array ou set
let sequenceArray = AnySequence(set)

protocol CapableOfCreatingASequence {
    //On ne peut pas renvoyer une sequence, pour l'instant, on ne peut pas typer les retour de protocol
    //func makeSequence() -> Sequence
    func makeSequence() -> AnySequence<Int>
    
}

protocol Additionning {
    associatedtype Operand
    
    //Fait une opération sur un type
    func add(_ lhs: Operand, _ rhs: Operand) -> Operand
}

struct AnyAdditioner<Operand> : Additionning {
    let base : (Operand, Operand) -> Operand
    
    init<A>(_ base: A) where A : Additionning, A.Operand == Operand {
        self.base = base.add
    }
    
    init(_ base: @escaping (Operand, Operand) -> Operand) {
        self.base = base
    }
    
    func add(_ lhs: Operand, _ rhs: Operand) -> Operand {
        return base(lhs,rhs)
    }
}

struct IntAdditioner : Additionning {
    
    typealias Operand = Int
    
    func add(_ lhs: Int, _ rhs: Int) -> Int {
        return lhs + rhs
    }
}


let additioner : AnyAdditioner<Int> = AnyAdditioner(IntAdditioner().add)
additioner.add(4, 5)
