//
//  Article.swift
//  demo
//
//  Created by Laurine Baillet on 28/11/2017.
//  Copyright © 2017 Laurine Baillet. All rights reserved.
//

import Foundation

public struct Article {
    public var title : String
    public var body : String
    public var date : Date
    
    //Par défaut init est privé, on est obligé de le redélcarer publique
    public init(title: String, body : String, date : Date) {
        self.title = title
        self.body = body
        self.date = date
    }
}
