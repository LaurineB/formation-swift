//: Playground - noun: a place where people can play

import UIKit

struct Article : Codable {
    let id : Int
    let body : String
    let date : Date
}

let article = Article(id: 1, body: "Body article", date: Date())
let encoder = JSONEncoder()

let data = try encoder.encode(article)

let string = String(bytes: data, encoding: .utf8)!
print(string)

let decoder = JSONDecoder()
let article2 = try decoder.decode(Article.self, from: data)

