//: Playground - noun: a place where people can play

import UIKit

class List<Element> : Sequence {
    let head: Element?
    let tail: List<Element>?
    
    init(head: Element?, tail: List<Element>?) {
        self.head = head
        self.tail = tail
    }
    
    init() {
        self.head = nil
        self. tail = nil
    }
    
    public func makeIterator() -> ListIterator<Element> {
        return ListIterator(list : self)
    }
}

class ListIterator<Element> : IteratorProtocol {
    var list : List<Element>?
    
    init(list: List<Element>) {
        self.list = list
    }
    
    func next() -> Element? {
        let element = list?.head
        self.list = list?.tail
        return element
    }
}

var list = List(head: 1, tail: List(head: 2, tail: List(head: 3, tail: nil)))
let array = Array(list)
